//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Carlos Lagares on 18/12/13.
//  Copyright (c) 2013 inVibe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
