//
//  main.m
//  Matchismo
//
//  Created by Carlos Lagares on 18/12/13.
//  Copyright (c) 2013 inVibe. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
