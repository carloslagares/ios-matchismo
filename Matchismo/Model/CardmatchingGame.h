//
//  CardmatchingGame.h
//  Matchismo
//
//  Created by Carlos Lagares on 18/12/13.
//  Copyright (c) 2013 inVibe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

//designated initializer
- (instancetype)initWithCardCount:(NSUInteger)count usingDeck: (Deck *)deck;

- (void) chooseCardAtIndex:(NSUInteger)index;
- (Card*)cardAtIndex:(NSUInteger)index;

@property (nonatomic, readonly) NSInteger score;

@end
